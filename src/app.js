import { Renderer } from "./renderer.js";
import { Game } from "./game.js";
import { PlayerInfo } from "./playerInfo.js";
import { GameMap } from "./gameMap.js";
import { gameCols, gameRows, stepIntervalMs } from "./constants.js";
import { Shape } from "./shape.js";

// We explicitly get the canvas, instead of relying on the variable that is
// automatically created, this allows us to handle the absence of this element
// however we want.
const canvas = document.getElementById("canvas");

if (!(canvas instanceof HTMLCanvasElement)) {
  throw new Error("Could not find canvas in HTML document");
}

const gameMap = new GameMap(gameCols, gameRows);
const game = new Game(gameMap);
const renderer = new Renderer(game, canvas.getContext("2d"));

const shape = new Shape(Shape.getRandomShapeType(), 1, gameCols / 2, 0, 0);
const playerInfo = new PlayerInfo(shape.playerId, shape);

game.set(shape.playerId, playerInfo);

setInterval(() => {
  game.step();
}, stepIntervalMs);

function renderLoop() {
  renderer.render();
  requestAnimationFrame(renderLoop);
}

renderLoop();
