import { airBlock, cellPixelSize, shapeColors } from "./constants.js";
import { Game } from "./game.js";
import { Shape } from "./shape.js";

/**
 *
 *
 * @param {Number} x The value to scale by the cellPixelSize
 * @returns
 */
function cellToPixel(x) {
  return x * cellPixelSize;
}

export class Renderer {
  /**
   * @param {Game} game
   * @param {CanvasRenderingContext2D} context
   */
  constructor(game, context) {
    this.game = game;
    this.context = context;
  }

  /**
   * Draws a shape to the renderer context
   *
   * @param {Shape} shape
   */
  #drawShape(shape) {
    // For now, all the shapes are blue.
    this.context.fillStyle = shapeColors.at(0);

    for (const [offsetX, offsetY] of shape.getCoordinates()) {
      const [blockX, blockY] = [offsetX + shape.col, offsetY + shape.row];

      this.#drawBlock(blockX, blockY);
    }
  }

  /**
   * Draws a block to the render context
   *
   * @param {Number} x
   * @param {Number} y
   */
  #drawBlock(x, y) {
    this.context.fillRect(
      cellToPixel(x),
      cellToPixel(y),
      cellPixelSize,
      cellPixelSize
    );
  }

  /**
   * Draws all grounded blocks to the render context
   */
  #drawGroundedBlocks() {
    // The requirements don't mention the colors that the grounded blocks should
    // have, we set it to blue for now.
    this.context.fillStyle = shapeColors.at(0);

    for (let col = 0; col < this.game.map.width; col++) {
      for (let row = 0; row < this.game.map.height; row++) {
        if (this.game.map.map[row][col] != airBlock) {
          this.#drawBlock(col, row);
        }
      }
    }
  }

  /**
   * Clears the context and draws all falling and dropped shapes.
   */
  render() {
    this.context.reset();
    this.game.forEachShape((shape) => this.#drawShape(shape));
    this.#drawGroundedBlocks();
  }
}
