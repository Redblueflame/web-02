import { Shape } from "./shape.js";
import { GameMap } from "./gameMap.js";
import { PlayerInfo } from "./playerInfo.js";

/**
 * @extends {Map<Number, PlayerInfo>}
 */
export class Game extends Map {
  /**
   * Creates a new game instance.
   * @param {GameMap} gameMap The game map in use during the game
   */
  constructor(gameMap) {
    super();
    this.map = gameMap;
    this.isGameOver = false;
  }

  /**
   * Returns shape of given player, or undefined if no such player or shape.
   * @param {Number} id Id of the player whose shape is to be returned.
   */
  getShape(id) {
    return this.get(id)?.getShape();
  }

  /**
   * Executes the provided function on each shape in the game.
   * @param {function(Shape):void} f The function to be executed. It takes a shape as unique parameters, and its return value is ignored.
   */
  forEachShape(f) {
    this.forEach((player) => f(player.getShape()));
  }

  /**
   * Tries to drop the given player's shape, i.e. move it down until it touches something if necessary, and then fixing it onto the map.
   * @param {Number} playerId The id of the player whose shape should be dropped
   */
  dropShape(playerId) {
    const shape = this.getShape(playerId);

    this.map.dropShape(shape);

    // NOTE: As we droped the shape in place, we are sure that the shape will
    // conflict with the current piece.
    this.forEachShape((otherShapes) => {
      const doesNotCollide = this.map.testShape(otherShapes);

      if (!doesNotCollide) {
        this.addNewShape(otherShapes.playerId);
      }
    });
  }

  /**
   * Advances the game by one step, i.e. moves all shapes down by one, drops any shape that was touching the ground, and replace it with a new one.
   */
  step() {
    if (this.isGameOver) {
      return;
    }

    /**
     * @type {Shape[]} Contains the shapes that should be placed on the
     * following game step.
     */
    const shapesToPlace = [];

    this.forEachShape((shape) => {
      const doesNotCollide = this.map.testShape(shape, shape.row + 1);

      if (doesNotCollide) {
        shape.row++;
      } else {
        shapesToPlace.push(shape);
      }
    });

    // Place all pieces
    shapesToPlace.forEach((shape) => {
      const currentPlayerShape = this.get(shape.playerId).getShape();

      /**
       * We check that the shape we are currently trying to drop is contained
       * inside the shapesToPlace we computed for the given step.
       * The shape could be absent from the array as the dropShape method also
       * changes the shapes for pieces that collides.
       */
      if (shapesToPlace.includes(currentPlayerShape)) {
        this.dropShape(shape.playerId);
      }
    });
  }

  /**
   * Replace current shape of given player id (if any) with a new random shape.
   * @param {Number} id Id of the player whose shape should be replaced.
   */
  addNewShape(id) {
    const shapeCol = this.map.width / 2;
    const shape = new Shape(Shape.getRandomShapeType(), id, shapeCol, 0, 0);

    this.get(id).setShape(shape);

    if (!this.map.testShape(shape)) {
      // This is the endddd
      this.gameOver();
      return;
    }
  }

  /**
   * Resets the game upon game over.
   */
  gameOver() {
    this.isGameOver = true;

    this.map.reset();

    this.forEach((player) => {
      this.addNewShape(player.getId());
    });
  }
}
