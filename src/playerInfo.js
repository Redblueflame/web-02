/**
 * Describes all information relative to a player.
 */
export class PlayerInfo {
  /**
   *
   * @param {Number} id
   * @param {Shape} shape
   */
  constructor(id, shape) {
    this.id = id;
    this.shape = shape;
  }

  /**
   * Gets the ID of the player
   * @returns {Number} The ID of the player
   */
  getId() {
    return this.id;
  }

  /**
   * Gets the current shape of a player
   * @returns {Shape} The current shape of the player
   */
  getShape() {
    return this.shape;
  }

  /**
   * Sets the shape for a given player.
   * @param {Shape} shape
   */
  setShape(shape) {
    this.shape = shape;
  }
}
