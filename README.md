[![Review Assignment Due Date](https://classroom.github.com/assets/deadline-readme-button-24ddc0f5d75046c5622901739e7c5dd533143b0c8e959d652212380cedb1ea36.svg)](https://classroom.github.com/a/Ddll2OBU)

# Tetris - Labo 1

Énoncé disponible
[ici](https://web-classroom.github.io/labos/labo-2-tetris-1.html).

Membre(s) du groupe :

- Alexandre Philibert (https://github.com/AlexandrePhilibert)
- Valentin Ricard (https://github.com/valentin-ricard)

A faire:

- Changer la logique du step pour matcher ce qui est écrit dans l'énoncé
- Mettre les pièces a undefined au game over, et les initialiser dans le clear (ou trouver une meilleure solution)
